###################
Project Name
###################

Job Portal

************
Installation
************

Via Command Line Interface (CLI): 

1. Clone the repository: https://gitlab.com/ManishBera/job-portal (git clone https://gitlab.com/ManishBera/job-portal)

2. Go to "application/config/database.php" file.

3. Under "$db['default']" configure the hostname, username, password, database.

4. Create a database (at phpMyAdmin) as per the above database configuration.

5. Go to "root/database" directory and import the "job_portal.sql".

6. Once setup done go to "http://localhost/job-portal/".   


************
Credentials
************

Candidate Login Details:

UserName: test-candidate

Email: testcandidate@email.com

Password: test-can@123

Employer Login Details:

UserName: test-employer

Email: testemployer@email.com

Password: test-emp@123

Both Candidate and Employer can login with the UserName or Email.