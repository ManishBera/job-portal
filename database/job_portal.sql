-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 23, 2024 at 07:05 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `job_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobs_list`
--

CREATE TABLE `jobs_list` (
  `id` int(11) NOT NULL,
  `employer_id` int(11) NOT NULL,
  `job_title` text COLLATE utf8_unicode_ci NOT NULL,
  `job_location` text COLLATE utf8_unicode_ci NOT NULL,
  `job_type` enum('1','2','3') COLLATE utf8_unicode_ci NOT NULL COMMENT '1=Full Time, 2=Part Time, 3=Remote',
  `job_category_id` int(11) NOT NULL,
  `job_description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `closing_date` date NOT NULL,
  `company_name` text COLLATE utf8_unicode_ci NOT NULL,
  `company_url` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Active,0=Inactive',
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jobs_list`
--

INSERT INTO `jobs_list` (`id`, `employer_id`, `job_title`, `job_location`, `job_type`, `job_category_id`, `job_description`, `closing_date`, `company_name`, `company_url`, `status`, `created_at`, `modified_at`) VALUES
(3, 2, 'PHP Developer', 'Saltlake, Sector-V', '1', 1, 'PHP, HTML, CSS', '2024-06-28', 'Company A', 'www.companya.com', '1', '2024-05-23 17:40:21', '0000-00-00 00:00:00'),
(4, 2, 'Web Designer', 'Jadavpur', '1', 2, 'PhotoShop, Figma, Adobe XD', '2024-07-31', 'Company B', 'https://www.company-b.org/', '1', '2024-05-23 17:42:29', '0000-00-00 00:00:00'),
(5, 2, 'Online Marketing Executive', 'New Town', '3', 3, 'Online Bidding, Co-ordination with Client and Team', '2024-07-10', 'Company C', 'www.companyc.in', '1', '2024-05-23 17:50:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `user_type` enum('C','E') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'C=Candidate, E=Employer',
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Active, 0=InActive',
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `email`, `password`, `user_type`, `status`, `created_at`, `modified_at`) VALUES
(1, 'Test Candidate', 'test-candidate', 'testcandidate@email.com', '3dce2d1b6c3b0484b786510c3a381f1a', 'C', '1', '2024-05-21 21:53:12', '2024-05-21 21:53:12'),
(2, 'Test Employer', 'test-employer', 'testemployer@email.com', '5d19f050e6fa275261abfb7f812ad1cd', 'E', '1', '2024-05-22 09:44:02', '2024-05-22 09:44:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobs_list`
--
ALTER TABLE `jobs_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `EmplyerID` (`employer_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobs_list`
--
ALTER TABLE `jobs_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jobs_list`
--
ALTER TABLE `jobs_list`
  ADD CONSTRAINT `EmplyerID` FOREIGN KEY (`employer_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
