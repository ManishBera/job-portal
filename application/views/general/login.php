        <div class="page_banner bg_cover" style="background-image: url(assets/images/page_banner.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="banner_content d-sm-flex align-items-center justify-content-between">
                            <div class="content">
                                <h3 class="page_title">Login</h3>
                                <!-- <p>More than 6 million jobseekers turn to Fitcareer in their search for work, making over 150,000 applications every day.</p> -->
                            </div> <!-- content -->                            
                        </div> <!-- banner content -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- page banner -->
    </header>
    <!--====== HEADER PART ENDS ======-->
    
    <!--====== LOGIN PART START ======-->
    <section class="login_area pt-80 pb-80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-8 col-sm-10">
                    <div class="login_form">
                        <form class="form-group" method="POST" action="<?php echo base_url('login'); ?>" >
                            <h4 class="login_title">Login </h4>                            
                            <div class="single_login">
                                <i class="fa fa-user"></i>
                                <input type="text" placeholder="Username/Email" id="username" name="username" />
                            </div>
                            
                            <div class="single_login">
                                <i class="fa fa-lock"></i>
                                <input type="password" placeholder="Password" id="password" name="password" />
                            </div> 
                            
                            <!--<div class="single_login checkbox">
                                <input id="checkbox1" type="checkbox">
                                <label for="checkbox1"></label>
                                <span>Keep Me Signed In</span>
                            </div>--> <!-- single login -->
                            
                            <div class="single_login">
                                <input type="submit" class="btn btn-primary form-control" name="loginBtn" value="LOGIN">
                            </div> <!-- single login -->
                            
                            <div class="single_login">
                                <p><a href="<?php echo base_url('register'); ?>">Don't have an account?</a></p>
                            </div> <!-- single login -->
                        </form>

                        <!-- Status message start -->
                        <?php  validation_errors();
                            if($this->session->flashdata('success_msg')){ 
                                echo '<p class="text-success">'.$this->session->flashdata('success_msg').'</p>'; 
                            }elseif($this->session->has_userdata('error_msg')){ 
                                echo '<p class="text-danger">'.$this->session->flashdata('error_msg').'</p>'; 
                            } 
                        ?>
                        <!-- Status message end -->

                    </div> <!-- login form -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
    <!--====== LOGIN PART ENDS ======-->   
    