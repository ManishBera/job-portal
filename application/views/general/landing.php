        <div class="header-banner d-lg-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="header-banner-content">
                            <h5 class="sub-title">A complete Job Portal for diverse people</h5>
                            <h2 class="main-title">Talent? Meet opportunity.</h2>

                            <div class="header-search">
                                <div class="search-form">
                                    <input type="text" placeholder="Search For opportunity">                                    
                                    <button class="main-btn">Search now <i class="fa fa-search"></i></button>
                                </div>
                                <div class="header-sign">
                                    <p><a href="<?php echo base_url('register'); ?>">Sign up now</a> to get started!</p>
                                </div>
                            </div>
                        </div> <!-- header banner content -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
            <div class="header-banner-image d-flex align-items-center justify-content-end">
                <div class="image">
                    <img src="<?php echo base_url('assets/images/header_banner.png') ?>" alt="header banner">
                </div> <!-- image -->
            </div> <!-- header banner image -->
        </div> <!-- header banner -->
    </header>
    <!--====== HEADER PART ENDS ======-->
    
    <!--====== COUNTER PART START ======-->
    <section class="counter_area gray-bg pt-50 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="single_counter text-center mt-30">
                        <div class="counter_icon_bar">
                            <div class="counter_icon">
                                <i class="flaticon-businessman"></i>
                            </div>
                            <div class="counter_num">
                                <span class="num">01</span>
                            </div>
                        </div>
                        <div class="counter_content">
                            <span class="count counter"><?php echo ($totalCompany)? $totalCompany : ""; ?></span>
                            <p>Registered Companies</p>
                        </div>
                    </div> <!-- single counter -->
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="single_counter text-center mt-30">
                        <div class="counter_icon_bar">
                            <div class="counter_icon">
                                <i class="flaticon-boy-broad-smile"></i>
                            </div>
                            <div class="counter_num">
                                <span class="num">02</span>
                            </div>
                        </div>
                        <div class="counter_content">
                            <span class="count counter"><?php echo ($totalCandidate)? $totalCandidate : ""; ?></span>
                            <p>Job Seekers</p>
                        </div>
                    </div> <!-- single counter -->
                </div>                
            </div> <!-- row -->
        </div> <!-- container -->
        <div class="counter-shape">
            <img src="assets/images/shape-1.svg" alt="Shape">
        </div>
    </section>
    <!--====== COUNTER PART ENDS ======-->     

    <!--====== TRENDING JOB PART START ======-->
    <section class="trending_jobs pt-75 pb-80 gray-bg">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="section_title pb-25">
                        <h5 class="sub_title">Explore</h5>
                        <h3 class="main_title">Recent Jobs</h3>
                    </div> <!-- section title -->
                </div>                
            </div> <!-- row -->

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="trending" role="tabpanel" aria-labelledby="trending-tab">
                    <div class="row">
                        <?php if(!empty($jobList)){ 
                                foreach($jobList as $jl_key=>$jl_val){
                            ?>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="single_jobs text-center mt-30">
                                        <div class="jobs_image">
                                            <?php 
                                                if($jl_val['job_category_id']==1){ $job_icon = "jobs-1.jpg";  }
                                                elseif($jl_val['job_category_id']==2){ $job_icon = "jobs-3.jpg"; }
                                                elseif($jl_val['job_category_id']==3){ $job_icon = "jobs-5.jpg"; }
                                                elseif($jl_val['job_category_id']==4){ $job_icon = "jobs-4.jpg"; }
                                            ?>
                                            <img src="<?php echo base_url("assets/images/$job_icon") ?>" alt="jobs">
                                        </div>
                                        <div class="jobs_content">
                                            <h4 class="jobs_title"><a href="job-details.html"><?php echo $jl_val['job_title']; ?></a></h4>
                                            <p class="sub_title"><?php echo $jl_val['company_name']; ?></p>
                                        </div>
                                        <div class="jobs_meta d-flex justify-content-between">
                                            <p class="location"><i class="fa fa-map-marker"></i> <?php echo $jl_val['job_location']; ?></p>
                                            <?php 
                                                if($jl_val['job_type']==1){ $job_contract_type = "Full Time"; }
                                                elseif($jl_val['job_type']==2){ $job_contract_type = "Part Time"; }
                                                elseif($jl_val['job_type']==3){ $job_contract_type = "Remote"; }
                                            ?>
                                            <p class="time"><i class="fa fa-clock-o"></i> <?php echo $job_contract_type; ?></p>
                                        </div>
                                        <?php if($this->session->userdata('userType')=="C"){ ?>
                                            <div class="jobs_btn">
                                                <a href="job-details.html" class="main-btn main-btn-2">apply now</a>
                                            </div>
                                        <?php } ?>    

                                    </div> <!-- single jobs -->
                                </div>
                        <?php } 
                            } else { 
                        ?>  
                            <div class="col-sm-12">
                                <h4 class="jobs_title">We have no recent jobs post. Stay updated with us!</h4>
                            </div>    
                        <?php } ?>                        
                    </div>
                </div> <!-- row -->
            </div>   

        </div> <!-- tab content -->
    </div> <!-- container -->
</section>
    <!--====== TRENDING JOB PART ENDS ======-->