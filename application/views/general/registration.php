        <div class="page_banner bg_cover" style="background-image: url(assets/images/page_banner.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="banner_content d-sm-flex align-items-center justify-content-between">
                            <div class="content">
                                <h3 class="page_title">Create Your account</h3>
                            </div> <!-- content -->                            
                        </div> <!-- banner content -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- page banner -->
    </header>
    <!--====== HEADER PART ENDS ======-->
    
    <!--====== REGISTRATION PART START ======-->
    <section class="login_area pt-80 pb-80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-8 col-sm-10">
                    <div class="login_form">
                        <form class="form-group" method="POST" action="<?php echo base_url('register'); ?>" >
                            <h4 class="login_title">Registration Form</h4>                           
                            
                            <div class="single_login">
                                <select class="form-control" id="user_type" name="user_type">
                                    <option value="">Select your preference</option>
                                    <option value="C" <?php if(!empty($userdata['user_type']) && $userdata['user_type'] === "C"){ echo 'selected'; } ?>>I am a Candidate</option>
                                    <option value="E" <?php if(!empty($userdata['user_type']) && $userdata['user_type'] === "E"){ echo 'selected'; } ?>>I am an Employer</option>
                                </select>
                            </div>

                            <div class="single_login">
                                <i class="fa fa-user"></i>
                                <input type="text" placeholder="Name" id="fullname" name="fullname" value="<?php echo !empty($userdata['name'])?$userdata['name']:''; ?>" required>
                            </div> 

                            <div class="single_login">
                                <i class="fa fa-user"></i>
                                <input type="text" placeholder="Username" id="username" name="username" value="<?php echo !empty($userdata['username'])?$userdata['username']:''; ?>" required>
                            </div> 
                            
                            <div class="single_login">
                                <i class="fa fa-envelope"></i>
                                <input type="text" placeholder="Email" id="email" name="email" value="<?php echo !empty($userdata['email'])?$userdata['email']:''; ?>" required>
                            </div> 
                            
                            <div class="single_login">
                                <i class="fa fa-lock"></i>
                                <input type="password" placeholder="Password" id="pwd" name="pwd">
                            </div> 
                            
                            <div class="single_login">
                                <i class="fa fa-lock"></i>
                                <input type="password" placeholder="Confirm Password" id="confpwd" name="confpwd">
                            </div> 
                            
                            <div class="single_login">
                                <input type="submit" class="btn btn-primary form-control" name="registerBtn" value="CREATE ACCOUNT">
                            </div> 
                            
                            <div class="single_login">
                                <p>Already have an account? <a href="<?php echo base_url('login'); ?>">Log In</a></p>
                            </div> 
                        </form>

                        <!-- Status message start -->
                        <?php  
                            if($this->session->flashdata('success_msg')){ 
                                echo '<p class="text-success">'.$this->session->flashdata('success_msg').'</p>'; 
                            }elseif($this->session->flashdata('error_msg')){ 
                                echo '<p class="text-danger">'.$this->session->flashdata('error_msg').'</p>'; 
                            } 
                        ?>
                        <!-- Status message end -->
                        
                    </div> <!-- REGISTRATION form -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
    <!--====== REGISTRATION PART ENDS ======-->