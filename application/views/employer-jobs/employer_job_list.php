<div class="page_banner bg_cover" style="background-image: url(assets/images/page_banner.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="banner_content d-sm-flex align-items-center justify-content-between">
                            <div class="content">
                                <h3 class="page_title">Welcome <?php echo ($userData['name']) ? $userData['name'] : "" ; ?></h3>
                            </div> <!-- content -->                            
                        </div> <!-- banner content -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- page banner -->
    </header>
    <!--====== HEADER PART ENDS ======-->
    
    <!--====== PROFILE PART START ======-->
    <section class="profile_area pt-30 pb-80">
        <div class="container">
            <div class="row">

                <!-- Profile Side Menubar Start -->
                <div class="col-lg-4 col-md-4">
                    <?php include APPPATH.'views/layout/profile_sidebar.php'; ?>                    
                </div>
                <!-- Profile Side Menubar Start -->

                <div class="col-lg-8 col-md-8">
                    <div class="profile_manage_jobs mt-50">
                        <h3 class="manage_jobs_title mb-20">Manage Jobs</h3>

                        <div class="manage_jobs table-responsive">  

                            <!-- Status message start -->
                            <?php  
                                if($this->session->flashdata('success_msg')){ 
                                    echo '<p class="text-success text-center small">'.$this->session->flashdata('success_msg').'</p>'; 
                                } elseif($this->session->has_userdata('error_msg')){ 
                                    echo '<p class="text-danger text-center small">'.$this->session->flashdata('error_msg').'</p>'; 
                                } 
                            ?>
                            <!-- Status message end -->

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><p>Name</p></th>
                                        <th><p>Contract Type</p></th>
                                        <th><p>Company</p></th>
                                        <th><p>Closing Date</p></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if(!empty($jobList)){ // Check for the JobList array is empty or not!  
                                            foreach($jobList as $jl_key=>$jl_val){
                                                $jobid = base64_encode($jl_val['id']);                                               
                                            ?>
                                                <tr>
                                                    <td class="name">
                                                        <div class="job_alert_name">
                                                            <h5 class="job_name"><?php echo $jl_val['job_title']; ?></h5>
                                                            <span class="location"><i class="fa fa-map-marker"></i> <?php echo $jl_val['job_location']; ?></span>
                                                        </div>
                                                    </td>
                                                    <td class="contract">
                                                        <?php 
                                                            if($jl_val['job_type']==1){ $job_contract_type = "Full Time"; $badgeColor="badge-primary"; }
                                                            if($jl_val['job_type']==2){ $job_contract_type = "Part Time"; $badgeColor="badge-secondary"; }
                                                            if($jl_val['job_type']==3){ $job_contract_type = "Remote"; $badgeColor="badge-warning"; }
                                                        ?>
                                                        <p class="badge badge-pill <?php echo $badgeColor; ?>"><?php echo $job_contract_type; ?></p>
                                                    </td>
                                                    <td class="candidates">
                                                        <?php echo $jl_val['company_name']; ?>
                                                    </td>    
                                                    <td class="candidates">
                                                        <?php echo date('d-F-Y', strtotime($jl_val['closing_date'])); ?>
                                                    </td> 
                                                    <td class="candidates mt-1">
                                                        <a href="<?php echo base_url("employer/job-list/$jobid"); ?>" title="Modify Job" ><i class="fa fa-edit fa-2x text-info"></i></a>
                                                    </td>
                                                    <td class="candidates mt-2 p-1">    
                                                        <a href="<?php echo base_url("employer/job-delete/$jobid"); ?>" title="Delete Job"><i class="fa fa-trash fa-2x text-danger"></i></a>
                                                    </td>                                                   
                                                </tr>
                                    <?php   }
                                        } else { 
                                    ?>
                                        <tr>
                                            <td class="text-center" colspan="5"><p class="small">There is no job in this list. Click <a href="<?php echo base_url('employer/job-post'); ?>">here</a> to post a new job.</p></td>
                                        </tr>    
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div> <!-- profile manage jobs -->                      
                    </div> <!-- profile manage resume -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
    <!--====== PROFILE PART ENDS ======-->