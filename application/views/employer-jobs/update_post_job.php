        <div class="page_banner bg_cover" style="background-image: url(<?php echo base_url('assets/images/page_banner.jpg'); ?>)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="banner_content d-sm-flex align-items-center justify-content-between">
                            <div class="content">
                                <h3 class="page_title">Welcome <?php echo ($userData['name']) ? $userData['name'] : "" ; ?></h3>
                            </div> <!-- content -->                            
                        </div> <!-- banner content -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- page banner -->
    </header>
    <!--====== HEADER PART ENDS ======-->

    <!--====== JobMate PART START ======-->
    <section class="post_job_area pt-80 pb-80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 col-lg-10">
                    <div class="post_job_form">
                        <form class="form-group" method="POST" action="<?php echo base_url('employer/job-update'); ?>" >
                            <!-- Job Details Section Start -->
                            <h4 class="post_job_title">Basic information</h4>
                            <div class="single_post_job">
                                <label>Job Title</label>
                                <input 
                                    type="text" 
                                    placeholder="Write job title" 
                                    id="job_title" 
                                    name="job_title"                                  
                                    value="<?php echo (!empty($jobDetails['job_title']))? $jobDetails['job_title'] : "" ; ?>"
                                    required />
                            </div> <!-- single resume -->                            

                            <div class="single_post_job">
                                <label>Location (optional)</label>
                                <input 
                                    type="text" 
                                    placeholder="e.g.Kolkata" 
                                    id="job_location" 
                                    name="job_location"
                                    value="<?php echo (!empty($jobDetails['job_location']))? $jobDetails['job_location'] : "" ; ?>"
                                    required />
                            </div> <!-- single resume -->

                            <div class="single_post_job">
                                <label>Job Category</label>
                                <select class="form-control" id="job_type" name="job_type" required>
                                    <option value="1" <?php echo ($jobDetails['job_type']==1)? "selected" : "" ; ?>>Full Time</option>
                                    <option value="2" <?php echo ($jobDetails['job_type']==2)? "selected" : "" ; ?>>Part Time</option>
                                    <option value="3" <?php echo ($jobDetails['job_type']==3)? "selected" : "" ; ?>>Remote</option>
                                </select>
                            </div> <!-- single resume -->   

                            <div class="single_post_job">
                                <label>Job Category</label>
                                <select class="form-control" id="job_cat" name="job_cat" required>
                                    <option value="">All Categories</option>
                                    <option value="1" <?php echo ($jobDetails['job_category_id']==1)? "selected" : "" ; ?>>Development</option>
                                    <option value="2" <?php echo ($jobDetails['job_category_id']==2)? "selected" : "" ; ?>>Design</option>
                                    <option value="3" <?php echo ($jobDetails['job_category_id']==3)? "selected" : "" ; ?>>Sale/Markting</option>
                                    <option value="4" <?php echo ($jobDetails['job_category_id']==4)? "selected" : "" ; ?>>SEO</option>
                                </select>
                            </div> <!-- single resume -->                            

                            <div class="single_post_job">
                                <label>Description</label>
                                <textarea id="job_desc" name="job_desc" required><?php echo ($jobDetails['job_description'])? $jobDetails['job_description'] : "" ; ?></textarea>
                            </div> <!-- single resume -->                            

                            <div class="single_post_job">
                                <label>Closing Date (optional)</label>
                                <input 
                                    type="date" 
                                    placeholder="yyyy-mm-dd" 
                                    class="form-control" 
                                    id="closing_date" 
                                    name="closing_date" 
                                    value="<?php echo (!empty($jobDetails['closing_date']))? $jobDetails['closing_date'] : "" ; ?>"
                                    required />
                            </div> <!-- single resume -->
                            <!-- Job Details Section End -->
                            
                            <!-- Company Details Section Start -->
                            <h4 class="post_job_title">Company Details</h4>
                            <div class="single_post_job">
                                <label>Company Name</label>
                                <input 
                                    type="text" 
                                    placeholder="Enter the name of the company" 
                                    id="company_name" 
                                    name="company_name" 
                                    value="<?php echo (!empty($jobDetails['company_name']))? $jobDetails['company_name'] : "" ; ?>"
                                    required />
                            </div> <!-- single resume -->

                            <div class="single_post_job">
                                <label>Website (optional)</label>
                                <input 
                                    type="text" 
                                    placeholder="http://" 
                                    id="company_url" 
                                    name="company_url"
                                    value="<?php echo (!empty($jobDetails['company_url']))? $jobDetails['company_url'] : "" ; ?>" />
                            </div> <!-- single resume -->                        
                            <!-- Company Details Section End -->

                            <div class="single_post_job">
                                <input type="submit" class="main-btn" name="jobDetailsUpdateBtn" value="Update JOB">
                                <input type="hidden" name="jid" value="<?php echo $jobID; ?>">
                            </div> <!-- single resume -->
                        </form>

                        <!-- Status message start -->
                        <?php  
                            if($this->session->flashdata('success_msg')){ 
                                echo '<p class="text-success">'.$this->session->flashdata('success_msg').'</p>'; 
                            } elseif($this->session->has_userdata('error_msg')){ 
                                echo '<p class="text-danger">'.$this->session->flashdata('error_msg').'</p>'; 
                            } 
                        ?>
                        <!-- Status message end -->

                    </div> <!-- JobMate form -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
    <!--====== JobMate PART ENDS ======-->