        <div class="page_banner bg_cover" style="background-image: url(assets/images/page_banner.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="banner_content d-sm-flex align-items-center justify-content-between">
                            <div class="content">
                                <h3 class="page_title">Welcome <?php echo ($userData['name']) ? $userData['name'] : "" ; ?></h3>
                            </div> <!-- content -->                            
                        </div> <!-- banner content -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- page banner -->
    </header>
    <!--====== HEADER PART ENDS ======-->
    
    <!--====== PROFILE PART START ======-->
    <section class="profile_area pt-30 pb-80">
        <div class="container">
            <div class="row">
                
                 <!-- Profile Side Menubar Start -->
                 <div class="col-lg-4 col-md-4">
                    <?php include APPPATH.'views/layout/profile_sidebar.php'; ?>                    
                </div>
                <!-- Profile Side Menubar Start -->

                <div class="col-lg-8 col-md-8">
                    <div class="profile_change_password mt-50">

                        <h4 class="profile_change_password_title">Change Password</h4>

                        <div class="change_password_content">
                            <form class="form-group" method="POST" action="<?php echo base_url('user/change-password'); ?>" >
                                <div class="single_password">
                                    <label>Old Password*</label>
                                    <input type="password" id="oldPass" name="oldPass" required />
                                </div> <!-- single password -->
                                <div class="single_password">
                                    <label>New Password*</label>
                                    <input type="password" id="newPass" name="newPass" required />
                                </div> <!-- single password -->
                                <div class="single_password">
                                    <label>Confirm New Password*</label>
                                    <input type="password" id="newConfPass" name="newConfPass" required />
                                </div> <!-- single password -->
                                <div class="single_password">
                                    <input type="submit" class="btn btn-primary form-control" name="chnagePasswordBtn" value="Save Changes">
                                </div> <!-- single password -->
                            </form>
                            
                            <!-- Status message start -->
                            <?php  validation_errors();
                                if($this->session->flashdata('success_msg')){ 
                                    echo '<p class="text-success">'.$this->session->flashdata('success_msg').'</p>'; 
                                } elseif($this->session->has_userdata('error_msg')){ 
                                    echo '<p class="text-danger">'.$this->session->flashdata('error_msg').'</p>'; 
                                } 
                            ?>
                            <!-- Status message end -->

                        </div> <!-- single job alert -->
                    </div> <!-- profile resume -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
    <!--====== PROFILE PART ENDS ======-->