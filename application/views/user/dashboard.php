        <div class="page_banner bg_cover" style="background-image: url(assets/images/page_banner.jpg)">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="banner_content d-sm-flex align-items-center justify-content-between">
                            <div class="content">
                                <h3 class="page_title">Welcome <?php echo ($userData['name']) ? $userData['name'] : "" ; ?></h3>
                            </div> <!-- content -->                            
                        </div> <!-- banner content -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- page banner -->
    </header>
    <!--====== HEADER PART ENDS ======-->
    
    <!--====== PROFILE PART START ======-->
    <section class="profile_area pt-30 pb-80">
        <div class="container">
            <div class="row">
                
                 <!-- Profile Side Menubar Start -->
                 <div class="col-lg-4 col-md-4">
                    <?php include APPPATH.'views/layout/profile_sidebar.php'; ?>                    
                </div>
                <!-- Profile Side Menubar Start -->

                <div class="col-lg-8 col-md-8">
                    <div class="profile_resume mt-50">
                        <div class="resume_author d-sm-flex">
                            <div class="author_image">
                                <img src="<?php echo base_url('assets/images/user-icon.png'); ?>" alt="author">
                            </div>
                            <div class="author_name media-body">
                                <h4 class="name_title">Mark Anderson</h4>
                                <p>UI/UX Designer</p>
                                <p> Mahattan, NYC, USA (+01) 211-123-5678</p>
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div> <!-- resume author -->
                        
                        <div class="resume_about">
                            <h3 class="resume_title">About Me</h3>
                            <p>Nullam semper erat arcu, ac tincidunt sem
 venenatis vel. Curabitur a dolor ac ligula fermentum eusmod ac 
ullamcorper nulla. Integer blandit uitricies aliquam. Pellentesque quis 
dui varius, dapibus vilit id, ipsum. Morbi ac eros feugiat, lacinia elit
 ut, elementum turpis. Curabitur justo sapien, tempus sit amet ruturm 
eu, commodo eu lacus. Morbi in ligula nibh. Maecenas ut mi at odio 
hendririt eleif end tempor vitae augue. Fusce eget arcu et nibh dapibus 
maximus consectetur in est. Sed iaculis Luctus nibh sed veneatis. </p>
                        </div> <!-- resume about -->
                        
                        <div class="resume_experience">
                            <h3 class="resume_title">Work Experience</h3>
                            
                            <div class="experience">
                                <h5 class="experience_title">UI/UX Designer</h5>
                                <h6 class="sub_title">Bannana INC.</h6>
                                <span>Fab 2017-Present(5year)</span>
                                <p>Lorem ipsum dolor sit amet, 
consectetur adipisicing elit. Libero vero, dolores, officia quibusdam 
architecto sapiente eos voluptas odit ab veniam porro quae possimus 
itaque, quas! Tempora sequi nobis, atque incidunt!</p>
                                <a class="projects" href="#">4 Projects</a>
                            </div> <!-- experience -->
                            
                            <div class="experience">
                                <h5 class="experience_title">UI Designer</h5>
                                <h6 class="sub_title">Whale Creative</h6>
                                <span>Fab 2017-Present(2year)</span>
                                <p>Lorem ipsum dolor sit amet, 
consectetur adipisicing elit. Libero vero, dolores, officia quibusdam 
architecto sapiente eos voluptas odit ab veniam porro quae possimus 
itaque, quas! Tempora sequi nobis, atque incidunt!</p>
                                <a class="projects" href="#">4 Projects</a>
                            </div> <!-- experience -->
                        </div> <!-- resume experience -->
                        
                        <div class="resume_education">
                            <h3 class="resume_title">Education</h3>
                            
                            <div class="education">
                                <h5 class="education_title">Massachusetts Institute Of Technology</h5>
                                <p>Bachelor of Computer Science</p>
                                <span>2010-2014</span>
                            </div> <!-- education -->
                            
                            <div class="education">
                                <h5 class="education_title">Massachusetts Institute Of Technology</h5>
                                <p>Bachelor of Computer Science</p>
                                <span>2010-2014</span>
                            </div> <!-- education -->
                        </div> <!-- resume education -->
                    </div> <!-- profile resume -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </section>
    <!--====== PROFILE PART ENDS ======-->