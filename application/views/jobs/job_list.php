<!DOCTYPE html>
<html class="js sizes customelements history pointerevents postmessage webgl websockets cssanimations csscolumns csscolumns-width csscolumns-span csscolumns-fill csscolumns-gap csscolumns-rule csscolumns-rulecolor csscolumns-rulestyle csscolumns-rulewidth no-csscolumns-breakbefore no-csscolumns-breakafter no-csscolumns-breakinside flexbox picture srcset webworkers" lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">

        <!--====== Title ======-->
        <title>JobMate | Job List</title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--====== Favicon Icon ======-->
        <link rel="shortcut icon" href="assets/images/favicon.png" type="image/png">

        <!--====== Flaticon CSS ======-->
        <link rel="stylesheet" href="assets/css/flaticon.css">

        <!--====== Font Awesome CSS ======-->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">

        <!--====== Nice Select CSS ======-->
        <link rel="stylesheet" href="assets/css/nice-select.css">

        <!--====== Bootstrap CSS ======-->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">

        <!--====== Default CSS ======-->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/default.css'); ?>">

        <!--====== Style CSS ======-->
        <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">

    </head>

    <body cz-shortcut-listen="true">
        <!--[if IE]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <!--====== PRELOADER PART START ======-->
        <div class="preloader" style="display: none;">
            <div class="loader">
                <div class="ytp-spinner">
                    <div class="ytp-spinner-container">
                        <div class="ytp-spinner-rotator">
                            <div class="ytp-spinner-left">
                                <div class="ytp-spinner-circle"></div>
                            </div>
                            <div class="ytp-spinner-right">
                                <div class="ytp-spinner-circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====== PRELOADER PART ENDS ======-->

        <!--====== HEADER PART START ======-->
        <header class="header-area">
            <div class="header-navbar sticky">
                <div class="container">
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="assets/images/logo.png" alt="Logo"></a>

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="toggler-icon"></span>
                            <span class="toggler-icon"></span>
                            <span class="toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto">
                                <li><a href="https://demo.graygrids.com/themes/jobmate/index.html">Home</a></li>
                                <li class="active">
                                    <a href="#">Page<button class="sub-nav-toggler"> <i class="fa fa-chevron-down"></i> </button></a>

                                    <ul class="sub-menu">
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/about.html">About</a></li>
                                        <li class="active"><a href="https://demo.graygrids.com/themes/jobmate/job-list.html">Job Page</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/job-details.html">Job Details</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/resume.html">Resume Page</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/privacy-policy.html">Privacy Policy</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/faq.html">FAQ</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/pricing.html">Pricing Tables</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/change-password.html">Change Password</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/notifications.html">Notifications</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/register.html">Register</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">Candidates<button class="sub-nav-toggler"> <i class="fa fa-chevron-down"></i> </button></a>
                                    
                                    <ul class="sub-menu">
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/browse-categories.html">Browse Categories</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/bookmarked.html">Bookmarked</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/add-resume.html">Add Resume</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/manage-resume.html">Manage Resumes</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/job-alerts.html">Job Alerts</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">Employers<button class="sub-nav-toggler"> <i class="fa fa-chevron-down"></i> </button></a>
                                    
                                    <ul class="sub-menu">
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/post-job.html">Post Job</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/manage-jobs.html">Manage Jobs</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/manage-applications.html">Manage Applications</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/browse-resumes.html">Browse Resumes</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="https://demo.graygrids.com/themes/jobmate/blog.html">Blog<button class="sub-nav-toggler"> <i class="fa fa-chevron-down"></i> </button></a>
                                    
                                    <ul class="sub-menu">
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/blog.html">Blog</a></li>
                                        <li><a href="https://demo.graygrids.com/themes/jobmate/blog-details.html">Blog Details</a></li>
                                    </ul>
                                </li>
                                <li><a href="https://demo.graygrids.com/themes/jobmate/contact.html">Contact</a></li>
                                <li><a href="https://demo.graygrids.com/themes/jobmate/login.html">Log In</a></li>

                                <li><a class="main-btn" href="https://demo.graygrids.com/themes/jobmate/post-job.html">Post a job</a></li>
                            </ul> <!-- navbar nav -->
                        </div>                    
                    </nav>
                </div> <!-- container -->
            </div> <!-- header navbar -->

            <div class="page_banner bg_cover" style="background-image: url(assets/images/page_banner.jpg)">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="banner_content d-sm-flex align-items-center justify-content-between">
                                <div class="content">
                                    <h3 class="page_title">Job List</h3>
                                </div> <!-- content -->                            
                            </div> <!-- banner content -->
                        </div>
                    </div> <!-- row -->
                </div> <!-- container -->
            </div> <!-- page banner -->
        </header>
        <!--====== HEADER PART ENDS ======-->

        <!--====== JOB LIST PART START ======-->
        <section class="job_list_area pt-80">
            <div class="container">
                <div class="job_list_filter">
                    <form action="#">
                        <div class="row">
                            <div class="col-lg-3 col-sm-6">
                                <div class="single_filter">
                                    <input type="text" placeholder="Job Keyword...">
                                </div> <!-- single filter -->
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="single_filter">
                                    <select style="display: none;">
                                        <option value="0" selected="selected">Location 01</option>
                                        <option value="1">Location 02</option>
                                        <option value="2">Location 03</option>
                                        <option value="3">Location 04</option>
                                    </select><div class="nice-select" tabindex="0"><span class="current">Location 01</span><ul class="list"><li data-value="0" class="option selected focus">Location 01</li><li data-value="1" class="option">Location 02</li><li data-value="2" class="option">Location 03</li><li data-value="3" class="option">Location 04</li></ul></div>
                                </div> <!-- single filter -->
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="single_filter">
                                    <select style="display: none;">
                                        <option value="0" selected="selected">Category 01</option>
                                        <option value="1">Category 02</option>
                                        <option value="2">Category 03</option>
                                        <option value="3">Category 04</option>
                                    </select><div class="nice-select" tabindex="0"><span class="current">Category 01</span><ul class="list"><li data-value="0" class="option selected focus">Category 01</li><li data-value="1" class="option">Category 02</li><li data-value="2" class="option">Category 03</li><li data-value="3" class="option">Category 04</li></ul></div>
                                </div> <!-- single filter -->
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="single_filter">
                                    <button class="main-btn">submit now</button>
                                </div> <!-- single filter -->
                            </div>
                        </div> <!-- row -->
                    </form>
                </div> <!-- job list filter -->
                <div class="job_list_main_content">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="sidebar_toggler d-lg-none">
                                <button id="sidebarCollapse" class="sidebar_btn"> Sidebar <i class="fa fa-bars"></i></button>
                            </div>
                            <div id="sidebar" class="job_list_sidebar">
                                <button id="dismiss" class="sidebar_close d-lg-none">
                                    <span></span>
                                    <span></span>
                                </button>
                                
                                <div class="single_sidebar">
                                    <div class="sidebar_title">
                                        <h4 class="title">Job Vacancy</h4>
                                    </div>
                                    <ul class="sidebar_content">
                                        <li><a href="#">Deputy Manage <span>25</span></a></li>
                                        <li><a href="#">Digital Accounting <span>25</span></a></li>
                                        <li><a href="#">Freelancer <span>25</span></a></li>
                                        <li><a href="#">Digital Accounting <span>25</span></a></li>
                                        <li><a href="#">Digital Accounting <span>25</span></a></li>
                                    </ul>
                                </div> <!-- single sidebar -->

                                <div class="single_sidebar">
                                    <div class="sidebar_title">
                                        <h4 class="title">Job Location</h4>
                                    </div>
                                    <ul class="sidebar_content">
                                        <li><a href="#">Australia <span>25</span></a></li>
                                        <li><a href="#">United States <span>25</span></a></li>
                                        <li><a href="#">Japan <span>25</span></a></li>
                                        <li><a href="#">Germany <span>25</span></a></li>
                                        <li><a href="#">California <span>25</span></a></li>
                                        <li><a href="#">Melbourne <span>25</span></a></li>
                                    </ul>
                                </div> <!-- single sidebar -->

                                <div class="single_sidebar">
                                    <div class="sidebar_title">
                                        <h4 class="title">Job Type</h4>
                                    </div>
                                    <ul class="sidebar_content">
                                        <li><a href="#">Freelance <span>25</span></a></li>
                                        <li><a href="#">Full Time <span>25</span></a></li>
                                        <li><a href="#">Internship <span>25</span></a></li>
                                        <li><a href="#">Part Time <span>25</span></a></li>
                                        <li><a href="#">Temporary <span>25</span></a></li>
                                    </ul>
                                </div> <!-- single sidebar -->
                            </div> <!-- job list sidebar -->
                        </div>
                        <div class="col-lg-8">
                            <div class="job_list_projects">
                                <div class="projects_top_bar d-sm-flex align-items-center justify-content-between">
                                    <div class="projects_show">
                                        <p>Showing 01 - 08 of 98 jobs</p>
                                    </div>
                                    <div class="projects_select">
                                        <select style="display: none;">
                                            <option value="0" selected="selected">Category 01</option>
                                            <option value="1">Category 02</option>
                                            <option value="2">Category 03</option>
                                            <option value="3">Category 04</option>
                                        </select><div class="nice-select" tabindex="0"><span class="current">Category 01</span><ul class="list"><li data-value="0" class="option selected focus">Category 01</li><li data-value="1" class="option">Category 02</li><li data-value="2" class="option">Category 03</li><li data-value="3" class="option">Category 04</li></ul></div>
                                    </div>
                                </div> <!-- projects top bar -->
                                <div class="job_projects">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="single_jobs jobs_border text-center mt-30">
                                                <div class="jobs_image">
                                                    <img src="JobMate_Job_List_files/jobs-1.jpg" alt="jobs">
                                                </div>
                                                <div class="jobs_content">
                                                    <h4 class="jobs_title"><a href="https://demo.graygrids.com/themes/jobmate/job-details.html">Python Developer</a></h4>
                                                    <p class="sub_title">Microsoft</p>
                                                </div>
                                                <div class="jobs_meta d-flex justify-content-between">
                                                    <p class="location"><i class="fa fa-map-marker"></i> 18 Brooklyn, NY</p>
                                                    <p class="time"><i class="fa fa-clock-o"></i> Full time</p>
                                                </div>
                                                <div class="jobs_btn">
                                                    <a href="https://demo.graygrids.com/themes/jobmate/job-details.html" class="main-btn main-btn-2">apply now</a>
                                                </div>
                                            </div> <!-- single jobs -->
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="single_jobs jobs_border text-center mt-30">
                                                <div class="jobs_image">
                                                    <img src="JobMate_Job_List_files/jobs-2.jpg" alt="jobs">
                                                </div>
                                                <div class="jobs_content">
                                                    <h4 class="jobs_title"><a href="https://demo.graygrids.com/themes/jobmate/job-details.html">Wordpress Developer</a></h4>
                                                    <p class="sub_title">Hooli</p>
                                                </div>
                                                <div class="jobs_meta d-flex justify-content-between">
                                                    <p class="location"><i class="fa fa-map-marker"></i> 18 Brooklyn, NY</p>
                                                    <p class="time"><i class="fa fa-clock-o"></i> Full time</p>
                                                </div>
                                                <div class="jobs_btn">
                                                    <a href="https://demo.graygrids.com/themes/jobmate/job-details.html" class="main-btn main-btn-2">apply now</a>
                                                </div>
                                            </div> <!-- single jobs -->
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="single_jobs jobs_border text-center mt-30">
                                                <div class="jobs_image">
                                                    <img src="JobMate_Job_List_files/jobs-3.jpg" alt="jobs">
                                                </div>
                                                <div class="jobs_content">
                                                    <h4 class="jobs_title"><a href="https://demo.graygrids.com/themes/jobmate/job-details.html">Business Analyst</a></h4>
                                                    <p class="sub_title">Facebook</p>
                                                </div>
                                                <div class="jobs_meta d-flex justify-content-between">
                                                    <p class="location"><i class="fa fa-map-marker"></i> 18 Brooklyn, NY</p>
                                                    <p class="time"><i class="fa fa-clock-o"></i> Full time</p>
                                                </div>
                                                <div class="jobs_btn">
                                                    <a href="https://demo.graygrids.com/themes/jobmate/job-details.html" class="main-btn main-btn-2">apply now</a>
                                                </div>
                                            </div> <!-- single jobs -->
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="single_jobs jobs_border text-center mt-30">
                                                <div class="jobs_image">
                                                    <img src="JobMate_Job_List_files/jobs-4.jpg" alt="jobs">
                                                </div>
                                                <div class="jobs_content">
                                                    <h4 class="jobs_title"><a href="https://demo.graygrids.com/themes/jobmate/job-details.html">Art Director</a></h4>
                                                    <p class="sub_title">Github</p>
                                                </div>
                                                <div class="jobs_meta d-flex justify-content-between">
                                                    <p class="location"><i class="fa fa-map-marker"></i> 18 Brooklyn, NY</p>
                                                    <p class="time"><i class="fa fa-clock-o"></i> Full time</p>
                                                </div>
                                                <div class="jobs_btn">
                                                    <a href="https://demo.graygrids.com/themes/jobmate/job-details.html" class="main-btn main-btn-2">apply now</a>
                                                </div>
                                            </div> <!-- single jobs -->
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="single_jobs jobs_border text-center mt-30">
                                                <div class="jobs_image">
                                                    <img src="JobMate_Job_List_files/jobs-5.jpg" alt="jobs">
                                                </div>
                                                <div class="jobs_content">
                                                    <h4 class="jobs_title"><a href="https://demo.graygrids.com/themes/jobmate/job-details.html">Creative Designer</a></h4>
                                                    <p class="sub_title">Google</p>
                                                </div>
                                                <div class="jobs_meta d-flex justify-content-between">
                                                    <p class="location"><i class="fa fa-map-marker"></i> 18 Brooklyn, NY</p>
                                                    <p class="time"><i class="fa fa-clock-o"></i> Full time</p>
                                                </div>
                                                <div class="jobs_btn">
                                                    <a href="https://demo.graygrids.com/themes/jobmate/job-details.html" class="main-btn main-btn-2">apply now</a>
                                                </div>
                                            </div> <!-- single jobs -->
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="single_jobs jobs_border text-center mt-30">
                                                <div class="jobs_image">
                                                    <img src="JobMate_Job_List_files/jobs-6.jpg" alt="jobs">
                                                </div>
                                                <div class="jobs_content">
                                                    <h4 class="jobs_title"><a href="https://demo.graygrids.com/themes/jobmate/job-details.html">React Developer</a></h4>
                                                    <p class="sub_title">PiedPiper</p>
                                                </div>
                                                <div class="jobs_meta d-flex justify-content-between">
                                                    <p class="location"><i class="fa fa-map-marker"></i> 18 Brooklyn, NY</p>
                                                    <p class="time"><i class="fa fa-clock-o"></i> Full time</p>
                                                </div>
                                                <div class="jobs_btn">
                                                    <a href="https://demo.graygrids.com/themes/jobmate/job-details.html" class="main-btn main-btn-2">apply now</a>
                                                </div>
                                            </div> <!-- single jobs -->
                                        </div>
                                    </div> <!-- row -->

                                    <ul class="pagination">
                                        <li class="page-item"><a class="active" href="#">01</a></li>
                                        <li class="page-item"><a href="#">02</a></li>
                                        <li class="page-item"><a href="#">03</a></li>
                                        <li class="page-item"><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                                    </ul> <!-- pagination -->
                                    
                                </div> <!-- job projects -->
                            </div> <!-- job list projects -->
                        </div>
                    </div> <!-- row -->
                </div> <!-- job list main content -->
            </div> <!-- container -->
        </section>
        <!--====== JOB LIST PART ENDS ======-->

        <!--====== SUBSCRIBE PART START ======-->
        <section class="subscribe_area pt-80 pb-80">
            <div class="container">
                <div class="subscribe_wrapper">
                    <div class="subscribe_shape_1">
                        <img src="JobMate_Job_List_files/shape-5.png" alt="shape">
                    </div> <!-- subscribe shape -->
                    <div class="row justify-content-center">
                        <div class="col-lg-8 col-md-10">
                            <div class="subscribe_content">
                                <div class="section_title text-center pb-25">
                                    <h5 class="sub_title">Newsletter</h5>
                                    <h3 class="main_title">Subscribe Newsletter &amp; Get Company News</h3>
                                </div> <!-- section title -->
                                <div class="subscribe_form">
                                    <input type="text" placeholder="Enter Email Address">
                                    <button class="main-btn">subscribe now</button>
                                </div>
                            </div> <!-- subscribe content -->
                        </div>
                    </div> <!-- row -->
                    <div class="subscribe_shape_2">
                        <img src="JobMate_Job_List_files/shape-4.png" alt="shape">
                    </div> <!-- subscribe shape -->
                </div> <!-- subscribe wrapper -->
            </div> <!-- container -->
        </section>
        <!--====== SUBSCRIBE PART ENDS ======-->

        <!--====== FOOTER PART START ======-->
        <footer class="footer_area gray-bg">     
            <div class="footer_copyright">
                <div class="container">
                    <div class="copyright_content text-center d-sm-flex justify-content-between align-items-center">
                        <a href="#" class="logo"><img src="JobMate_Job_List_files/logo.png" alt="Logo"></a>
                        <p class="copyright">JobMate © 2024 All Right Reserved</p>
                    </div> <!-- copyright content -->
                </div> <!-- container -->
            </div> <!-- footer copyright -->
        </footer>
        <!--====== FOOTER PART ENDS ======-->

        <!--====== BACK TOP TOP PART START ======-->
        <a href="#" class="back-to-top" style="display: none;"><i class="fa fa-chevron-up"></i></a>
        <!--====== BACK TOP TOP PART ENDS ======-->

        <!--====== OVERLAY PART START ======-->    
        <div class="overlay"></div>    
        <!--====== OVERLAY PART ENDS ======-->    

        <!--====== Jquery js ======-->
        <script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
        <script src="assets/js/vendor/modernizr-3.7.1.min.js"></script>

        <!--====== Bootstrap js ======-->
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>


        <!--====== Nice Select js ======-->
        <script src="assets/js/jquery.nice-select.min.js"></script>

        <!--====== Counter Up js ======-->
        <script src="assets/js/waypoints.min.js"></script>
        <script src="assets/js/jquery.counterup.js"></script>

        <!--====== Main js ======-->
        <script src="assets/js/main.js"></script>

    </body>
</html>