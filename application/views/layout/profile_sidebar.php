<div class="profile_sidebar mt-50">
    <div class="profile_title">
        <h3 class="title">Manage Account</h3>
    </div>
    <div class="profile_list">
        <ul>
            <?php if($this->session->userdata('userType')==="C"){ // Candidate Profile Menu ?>
                <li><a class="<?php if($this->uri->uri_string() == 'dashboard'){ echo 'active'; } ?>" href="<?php echo base_url('dashboard'); ?>">My Resume</a></li>
                <li><a class="<?php if($this->uri->uri_string() == 'candidate/manage-resume'){ echo 'active'; } ?>" href="<?php echo base_url('candidate/manage-resume'); ?>">Manage Resume</a></li>
                <li><a class="<?php //if($this->uri->uri_string() == ''){ echo 'active'; } ?>" href="<?php //echo base_url(''); ?>">Applied Jobs</a></li>
                <li><a class="<?php if($this->uri->uri_string() == 'user/change-password'){ echo 'active'; } ?>" href="<?php echo base_url('user/change-password'); ?>">Change Password</a></li>
                <li><a class="text-danger" href="<?php echo base_url('signout'); ?>">Sing Out</a></li>
            <?php } else if($this->session->userdata('userType')==="E"){ // Employer Profile Menu ?>                 
                <li><a class="fw-bold" href="<?php echo base_url('employer/job-post'); ?>">Post a Job</a></li>
                <li><a class="<?php if($this->uri->uri_string() == 'employer/job-list'){ echo 'active'; } ?>" href="<?php echo base_url('employer/job-list'); ?>">Manage Jobs</a></li>
                <li><a class="<?php //if($this->uri->uri_string() == 'employer/manage-application'){ echo 'active'; } ?>" href="<?php //echo base_url('dashboard'); ?>">Manage Applications</a></li>
                <li><a class="<?php if($this->uri->uri_string() == 'user/change-password'){ echo 'active'; } ?>" href="<?php echo base_url('user/change-password'); ?>">Change Password</a></li>
                <li><a class="text-danger" href="<?php echo base_url('signout'); ?>">Sign Out</a></li>
            <?php } ?>
            
        </ul>
    </div>
</div> <!-- profile sidebar -->