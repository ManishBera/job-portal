        <!--====== FOOTER PART START ======-->
        <footer class="footer_area gray-bg">       
            <div class="footer_copyright">
                <div class="container">
                    <div class="copyright_content text-center d-sm-flex justify-content-between align-items-center">
                        <a href="#" class="logo"><img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="Logo"></a>
                        <p class="copyright">Job Portal © <?php echo date('Y'); ?> All Right Reserved</p>
                    </div> <!-- copyright content -->
                </div> <!-- container -->
            </div> <!-- footer copyright -->
        </footer>
        <!--====== FOOTER PART ENDS ======-->

        <!--====== BACK TOP TOP PART START ======-->
        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        <!--====== BACK TOP TOP PART ENDS ======-->
    
        <!--====== Jquery js ======-->
        <script src="<?php echo base_url('assets/js/vendor/jquery-1.12.4.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/vendor/modernizr-3.7.1.min.js'); ?>"></script>

        <!--====== Bootstrap js ======-->
        <script src="<?php echo base_url('assets/js/popper.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>


        <!--====== Nice Select js ======-->
        <!--<script src="<?php // echo base_url('assets/js/jquery.nice-select.min.js'); ?>"></script>-->

        <!--====== Counter Up js ======-->
        <script src="<?php echo base_url('assets/js/waypoints.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.counterup.js'); ?>"></script>

        <!--====== Main js ======-->
        <script src="<?php echo base_url('assets/js/main.js'); ?>"></script>

    </body>
</html>