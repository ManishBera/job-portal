    <!--====== HEADER PART START ======-->
    <header class="header-area header_shadow">
        <div class="header-navbar">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="Logo"></a>
                    
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="toggler-icon"></span>
                        <span class="toggler-icon"></span>
                        <span class="toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="<?php if($this->uri->uri_string() == ''){ echo 'active'; } ?>"><a href="<?php echo base_url(); ?>">Home</a></li>                            
                            <li><a href="<?php echo base_url('job-list'); ?>">Browse Jobs</a></li>                            
                            
                            <?php if(!empty($this->session->userdata('userID'))) { ?>
                                <li>
                                    <a href="#">Manage Account</a>                                
                                        <ul class="sub-menu">
                                            <?php if($this->session->userdata('userType')==="C") { //Candidate Navigation Menu ?>
                                                <li><a class="<?php if($this->uri->uri_string() == 'dashboard'){ echo 'active'; } ?>" href="<?php echo base_url('dashboard'); ?>">My Resume</a></li>
                                                <li><a class="<?php if($this->uri->uri_string() == 'candidate/manage-resume'){ echo 'active'; } ?>" href="<?php echo base_url('candidate/manage-resume'); ?>">Manage Resume</a></li>
                                                <li><a class="<?php //if($this->uri->uri_string() == 'candidate/manage-resume'){ echo 'active'; } ?>" href="<?php //echo base_url('dashboard'); ?>">Applied Jobs</a></li>
                                                <li><a class="<?php if($this->uri->uri_string() == 'user/change-password'){ echo 'active'; } ?>" href="<?php echo base_url('user/change-password'); ?>">Change Password</a></li>
                                                <li><a class="text-danger" href="<?php echo base_url('signout'); ?>">Sing Out</a></li>
                                            <?php } else if($this->session->userdata('userType')==="E") { //Employer Navigation Menu ?> 
                                                <li><a class="<?php if($this->uri->uri_string() == 'employer/job-list'){ echo 'active'; } ?>" href="<?php echo base_url('employer/job-list'); ?>">Manage Jobs</a></li>
                                                <li><a class="<?php //if($this->uri->uri_string() == 'user/change-password'){ echo 'active'; } ?>" href="<?php //echo base_url('dashboard'); ?>">Manage Applications</a></li>
                                                <li><a class="<?php if($this->uri->uri_string() === 'user/change-password'){ echo 'active'; } ?>" href="<?php echo base_url('user/change-password'); ?>">Change Password</a></li>
                                                <li><a class="text-danger" href="<?php echo base_url('signout'); ?>">Sign Out</a></li>
                                            <?php } ?>          
                                        </ul>
                                </li>
                            <?php } ?>                       
                            
                            <?php if($this->session->userdata('userID')=="") { ?>
                                <li><a class="text-primary fw-bold" href="<?php echo base_url('login'); ?>">Sign in | Register</a></li>
                            <?php } ?> 

                            <?php if($this->session->userdata('userType')==="E") { // Enabled for Employers Only ?>
                                <li><a class="main-btn" href="<?php echo base_url('employer/job-post'); ?>">Post a job</a></li>
                            <?php } ?>    
                        </ul> <!-- navbar nav -->
                    </div>                
                </nav>
            </div> <!-- container -->
        </div> <!-- header navbar -->
        