<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct() { 
        parent::__construct(); 
         
        // Load form validation Library & Model(s) 
        $this->load->library(array('form_validation')); 
        $this->load->model('user');   
		
		// Retrive the UserId from session & store into a variable
		$this->userid = $this->session->userdata('userID');
    } 

    /*
		Description: User Dashboard
		Purpose: Show user Dashboard
		Created By: Manish Bera
		Date of Creation: 22-May-2024
	*/
	public function dashboard()
	{
		$data = array();

		if($this->userid){
			// Get User details by UserID
			$data['userData'] = $this->user->getUserDetails($this->userid); 
					
			if($this->session->userdata('userType')=="C"){ // Check if the user is Candidate 
				// Load the User Dashboard Screen	
				$this->load->view('layout/header');
				$this->load->view('layout/nav');
				$this->load->view('user/dashboard', $data);
				$this->load->view('layout/footer');
			} else if($this->session->userdata('userType')=="E"){ // Check if the user is Employer 
				//Redirect to the Job List Page
				redirect('employer/job-list');
			}				

		} else {
			// Redirected to Login Page (if user not looged-in)
			redirect('login', 'refresh');
		}
	}

	/*
		Description: User Change-Password after Login
		Purpose: Change User Password
		Created By: Manish Bera
		Date of Creation: 22-May-2024
	*/
	public function changePassword()
	{
		$data   = array(); 
        
		if($this->userid){ 		
			// Get User details by UserID
			$data['userData'] = $this->user->getUserDetails($this->userid);

			if($this->input->post('chnagePasswordBtn')){  // For Submitted the Form
				
				// Old Password user-input
				$oldPass = $this->security->xss_clean(strip_tags(trim($this->input->post('oldPass')))); 

				// Check the Old-Password is Matched or Not
				$chkUserPass = $this->user->chkUserPassword($oldPass);

				if($chkUserPass===true){ // If OldPassword matched then process for the new password change	
					
					//Setup the Validation rules for the form fields
					$validationRules = array(
												array(
														'field' => 'newPass',
														'label' => 'New Password',
														'rules' => 'required'
													),
												array(
														'field' => 'newConfPass',
														'label' => 'New Confirm password',
														'rules' => 'required|matches[newPass]'
													)
											);

					// Set the Validation Rules to the form
					$this->form_validation->set_rules($validationRules);	
					
					if($this->form_validation->run() == true){ // If Validation Success
						$userData['password'] = md5($this->security->xss_clean(strip_tags(trim($this->input->post('newPass')))));					

						// Upadate User Record
						$updateUserDetails = $this->user->updateUserDetails($userData);

						if($updateUserDetails === true){
							$this->session->set_flashdata('success_msg', 'Password has been changed successfully.');
							redirect('user/change-password');
						} else {
							$this->session->set_flashdata('error_msg', 'Unable to change the password right now. Please try after sometime!');
							redirect('user/change-password');
						}
					}

				} else {
					$this->session->set_flashdata('error_msg', 'Old password does not matched!');
					redirect('user/change-password');
				}	
			} else {			
				// Load the User Change Password Screen
				$this->load->view('layout/header');
				$this->load->view('layout/nav');
				$this->load->view('user/change_password', $data);
				$this->load->view('layout/footer');
			}
		} else {
			// Redirected to Login Page (if user not looged-in)
			redirect('login', 'refresh');
		}	
	}

    /*
		Description: Manage Candidate Resume
		Purpose: Candidate Resume Edit View & Update
		Created By: Manish Bera
		Date of Creation: 22-May-2024
	*/
	public function manageResume()
	{
        $data   = $userDetails = array(); 

        if($this->userid){
			// Get User details by UserID
			$data['userData'] = $this->user->getUserDetails($this->userid);

			if($this->input->post('updateResumeBtn')){

			} else {			
				// Load the User Change Password Screen
				$this->load->view('layout/header');
				$this->load->view('layout/nav');
				$this->load->view('candidate/manage_resume', $data);
				$this->load->view('layout/footer');
			}
		} else {
			// Redirected to Login Page (if user not looged-in)
			redirect('login', 'refresh');
		}
    }
}