<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends CI_Controller {
	
	public function __construct() { 
        parent::__construct(); 
         
        // Load form validation Library & Model(s) 
        $this->load->library(array('form_validation')); 
        $this->load->model(array('user', 'job'));     
		
		// Retrive the UserId from session & store into a variable
		$this->userid = $this->session->userdata('userID');
    } 
	
	/*
		Description: Job Listing Page 
		Purpose: Show the list of jobs 
		Created By: Manish Bera
		Date of Creation: 21-May-2024
	*/
	public function index()
	{
		$this->load->view('jobs/job_list');
	}

	/*
		Description: Job Details Page
		Purpose: Show the individual job details page by JobID
		Created By: Manish Bera
		Date of Creation: 21-May-2024
	*/
	public function jobDetails($job_id=null)
	{
		$this->load->view('jobs/job_details');
	}

	/*
		Description: Post a New Job 
		Purpose: New Job form screen and Store New Job  
		Created By: Manish Bera
		Date of Creation: 22-May-2024
	*/
	public function newJobPost()
	{
		$data = array();		

		if($this->userid){

			// Get User details by UserID
			$data['userData'] = $this->user->getUserDetails($this->userid);

			if($this->input->post('jobPostBtn')){

				// New Job Data Submitted Inputs
				$jobPostData['employer_id']		= $this->userid;
				$jobPostData['job_title'] 		= $this->security->xss_clean(strip_tags(trim($this->input->post('job_title')))); 
				$jobPostData['job_location'] 	= $this->security->xss_clean(strip_tags(trim($this->input->post('job_location')))); 
				$jobPostData['job_type'] 		= $this->security->xss_clean(strip_tags(trim($this->input->post('job_type')))); 
				$jobPostData['job_category_id'] = $this->security->xss_clean(strip_tags(trim($this->input->post('job_cat')))); 
				$jobPostData['job_description'] = $this->security->xss_clean(strip_tags(trim($this->input->post('job_desc')))); 
				$jobPostData['closing_date'] 	= $this->security->xss_clean(strip_tags(trim($this->input->post('closing_date')))); 
				$jobPostData['company_name'] 	= $this->security->xss_clean(strip_tags(trim($this->input->post('company_name')))); 
				$jobPostData['company_url'] 	= $this->security->xss_clean(strip_tags(trim($this->input->post('company_url')))); 
				$jobPostData['status'] 			= 1; 
				$jobPostData['created_at'] 		= date('Y-m-d H:i:s'); 

				// Save the New Job Data to Database
				$saveJobDetails = $this->job->addNewJobDetails($jobPostData);	

				if($saveJobDetails){
					$this->session->set_flashdata('success_msg', 'One new job has been posted by you. 
																  Please see the list below.');
					redirect('employer/job-list');
				} else {
					$this->session->set_flashdata('error_msg', 'Unable to post the new job right now.');
					redirect('employer/job-post');
				}
				
			} else {
				// Load the Post New Job Screen
				$this->load->view('layout/header');
				$this->load->view('layout/nav');
				$this->load->view('employer-jobs/post_job', $data);
				$this->load->view('layout/footer');
			}

		} else {
			// Redirected to Login Page (if user not looged-in)
			redirect('login','refresh'); 
		}
	}	
	
	/*
		Description: Job List for an Employer 
		Purpose: Employer can see & manage all the posted-job by themselves
		Created By: Manish Bera
		Date of Creation: 22-May-2024
	*/
	public function jobList(){
		$data = array();		

		if($this->userid){

			// Retrive User details by UserID
			$data['userData'] = $this->user->getUserDetails($this->userid);

			// Retrive Job List by UserID
			$data['jobList'] = $this->job->getEmployerJobList();

			// Load the Employer Job-List Screen
			$this->load->view('layout/header');
			$this->load->view('layout/nav');
			$this->load->view('employer-jobs/employer_job_list', $data);
			$this->load->view('layout/footer');

		} else {
			// Redirected to Login Page (if user not looged-in)
			redirect('login','refresh'); 
		}
	}

	/*
		Description: Job List for an Employer 
		Purpose: Employer can see & manage all the posted-job by themselves
		Created By: Manish Bera
		Date of Creation: 22-May-2024
	*/
	public function jobDetailsForModify(){
		$data = array();		

		if($this->userid){

			// Retrive User details by UserID
			$data['userData'] = $this->user->getUserDetails($this->userid);

			// Capture the JobID from URL
			$data['jobID'] = $this->security->xss_clean(trim($this->uri->segment(3)));
			$jobID = base64_decode($data['jobID']);

			// Rerive the Job Details by using JobID
			$data['jobDetails'] = $this->job->getJobDetails($jobID);

			// Load the Posted Job Screen with Job-Details
			$this->load->view('layout/header');
			$this->load->view('layout/nav');
			$this->load->view('employer-jobs/update_post_job', $data);
			$this->load->view('layout/footer');

		} else {
			// Redirected to Login Page (if user not looged-in)
			redirect('login','refresh'); 
		}
	}

	/*
		Description: Modify a Posted Job 
		Purpose: Get the updated job-details entries & update to database  
		Created By: Manish Bera
		Date of Creation: 23-May-2024
	*/
	public function updateJobPost()
	{
		$data = array();		

		if($this->userid){

			// Get User details by UserID
			$data['userData'] = $this->user->getUserDetails($this->userid);

			if($this->input->post('jobDetailsUpdateBtn')){

				// Capture JobID from hidden input
				$jobIDEncoded 	= $this->security->xss_clean(strip_tags(trim($this->input->post('jid'))));
				$jobID	= base64_decode($jobIDEncoded); // Decoded the jobID

				// Job Data Submitted Inputs
				$jobUpdateData['job_title']			= $this->security->xss_clean(strip_tags(trim($this->input->post('job_title')))); 
				$jobUpdateData['job_location'] 		= $this->security->xss_clean(strip_tags(trim($this->input->post('job_location')))); 
				$jobUpdateData['job_type'] 			= $this->security->xss_clean(strip_tags(trim($this->input->post('job_type')))); 
				$jobUpdateData['job_category_id'] 	= $this->security->xss_clean(strip_tags(trim($this->input->post('job_cat')))); 
				$jobUpdateData['job_description'] 	= $this->security->xss_clean(strip_tags(trim($this->input->post('job_desc')))); 
				$jobUpdateData['closing_date'] 		= $this->security->xss_clean(strip_tags(trim($this->input->post('closing_date')))); 
				$jobUpdateData['company_name'] 		= $this->security->xss_clean(strip_tags(trim($this->input->post('company_name')))); 
				$jobUpdateData['company_url'] 		= $this->security->xss_clean(strip_tags(trim($this->input->post('company_url')))); 
				$jobUpdateData['modified_at']		= date('Y-m-d H:i:s'); 

				// Update the Job Data to Database
				$updateJobDetails = $this->job->updateJobDetails($jobUpdateData, $jobID);	

				if($updateJobDetails){ // For success job updatation
					$this->session->set_flashdata('success_msg', 'Job details has been updated successfully.');
					redirect('employer/job-list/'.$jobIDEncoded);
				} else {
					$this->session->set_flashdata('error_msg', 'Unable to update the job details right now. Please try after sometimes!');
					redirect('employer/job-list/'.$jobIDEncoded);
				}
				
			} else {	
				$this->session->set_flashdata('error_msg', 'Please submit the data properly!');
				redirect('employer/job-list/'.$jobIDEncoded);
			}

		} else {
			// Redirected to Login Page (if user not looged-in)
			redirect('login','refresh'); 
		}
	}

	/*
		Description: Delete a Posted Job from Employer Panel
		Purpose: Completely delete the job from the Employer Panel  
		Created By: Manish Bera
		Date of Creation: 23-May-2024
	*/
	public function deleteJobPost()
	{
		$data = array();		

		if($this->userid){
			// Capture JobID from hidden input
			$jobIDEncoded 	= $this->security->xss_clean(trim($this->uri->segment(3)));
			$jobID	= base64_decode($jobIDEncoded); // Decoded jobID

			// Delete the Complete Job-Data from Database
			$detleteJobDetails = $this->job->deleteJobDetails($jobID);	

			if($detleteJobDetails){ // For success job deletion
				$this->session->set_flashdata('success_msg', 'You have deleted a job successfully.');
				redirect('employer/job-list');
			} else {
				$this->session->set_flashdata('error_msg', 'Unable to delete the job right now. Please try after sometimes!');
				redirect('employer/job-list');
			}

		} else {
			// Redirected to Login Page (if user not looged-in)
			redirect('login','refresh'); 
		}
	}



} // Ending of Job Controller Class
