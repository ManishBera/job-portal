<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends CI_Controller {

	public function __construct() { 
        parent::__construct(); 
         
        // Load form validation Library & Model(s) 
        $this->load->library(array('form_validation')); 
        $this->load->model(array('user','job'));  
		
		// Retrive the UserId from session & store into a variable
		$this->userid = $this->session->userdata('userID');
    } 

	/*
		Description: Website Landing Page
		Purpose: Show the website landing page
		Created By: Manish Bera
		Date of Creation: 21-May-2024
	*/
	public function index()
	{
		$data = array();

		// Retrive the Job List from Database
		$data['jobList'] = $this->job->getJobList($limit=6, $orderBy="desc");

		//Total Company Count
		$data['totalCompany'] = $this->job->getTotalCompanyCount();

		//Total Candidate Count
		$data['totalCandidate'] = $this->user->getTotalUserCount($userType="C");
		
		// Load the Landing Page view
		$this->load->view('layout/header');
		$this->load->view('layout/nav');
		$this->load->view('general/landing', $data);
		$this->load->view('layout/footer');
	}

	/*
		Description: Website Registratiob Page
		Purpose: Show the website registration page
		Created By: Manish Bera
		Date of Creation: 21-May-2024
	*/
	public function registration()
	{
		$data = $userDetails = array();

		if($this->input->post('registerBtn')){
			
			//Setup the Validation rules for the form fields
			$validationRules = array(
										array(
												'field' => 'fullname',
												'label' => 'Name',
												'rules' => 'required'
										),
										array(
												'field' => 'username',
												'label' => 'Username',
												'rules' => 'required'
										),
										array(
											'field' => 'email',
											'label' => 'Email',
											'rules' => 'required|valid_email|callback_email_check'
										),
										array(
												'field' => 'pwd',
												'label' => 'Password',
												'rules' => 'required',
												'errors' => array(
													'required' => 'You must provide a %s.',
												),
										),
										array(
												'field' => 'confpwd',
												'label' => 'Password Confirmation',
												'rules' => 'required|matches[pwd]'
										),
										array(
												'field' => 'user_type',
												'label' => 'User Type',
												'rules' => 'required',

										)
									);
			
			// Set the Validation Rules to the form
			$this->form_validation->set_rules($validationRules);

			$data['userdata'] = $userDetails = array(
														'name' 		=> 	$this->security->xss_clean(strip_tags(trim($this->input->post('fullname')))),
														'username' 	=> 	$this->security->xss_clean(strip_tags(trim($this->input->post('username')))),
														'email' 	=> 	$this->security->xss_clean(strip_tags(trim($this->input->post('email')))),
														'password' 	=> 	md5($this->security->xss_clean(strip_tags(trim($this->input->post('pwd'))))),
														'user_type' => 	$this->security->xss_clean(strip_tags(trim($this->input->post('user_type')))),
														'status ' 	=> 	1
												);
			
			if($this->form_validation->run() == true){ 

				// Insert data to Database through Model function
				$insert = $this->user->insert($userDetails); 

                if($insert){ 
                    $succMsg = 'Your account registration has been successful. Please login to your account.';
					$this->session->set_flashdata('success_msg', $succMsg);

					// Redirect to Login page after successfull registration
					redirect('login', 'refresh'); 

				} else{ 
                    $errMsg = 'Some problems occured, please try again.'; 
					$this->session->set_flashdata('error_msg', $errMsg);
					redirect('register');
                } 
			} else {
				$errMsg = 'Please fill all the mandatory fields.';
				$this->session->set_flashdata('error_msg', $errMsg);
				redirect('register');
			}

		} else {
			// Load the view
			$this->load->view('layout/header');
			$this->load->view('layout/nav');
			$this->load->view('general/registration', $data);
			$this->load->view('layout/footer');
		}
		
		
	}

	/*
		Description: Duplicate email Checker
		Purpose: Check wheather the email address is present or not
		Created By: Manish Bera
		Date of Creation: 21-May-2024
	*/
    public function email_check($str){         

        $checkEmail = $this->user->getRows($str); 

        if($checkEmail > 0){ 
            $this->form_validation->set_message('email_check', 'The given email already exists.'); 
            return false; 
        }else{ 
            return true; 
        } 
    }
	
	/*
		Description: Website Login Page
		Purpose: Show the website login page
		Created By: Manish Bera
		Date of Creation: 21-May-2024
	*/
	public function login()
	{
		$data = array();

		if($this->input->post('loginBtn')){
			
			//Setup the Validation rules for the form fields
			$validationRules = array(
										array(
												'field' => 'username',
												'label' => 'Username',
												'rules' => 'required'
										),
										array(
											'field' => 'password',
											'label' => 'Password',
											'rules' => 'required'
										)
									);

			// Set the Validation Rules to the form
			$this->form_validation->set_rules($validationRules);			
			
			if($this->form_validation->run() == true){ 
				$userData['username'] = $this->security->xss_clean(strip_tags(trim($this->input->post('username'))));
				$userData['password'] = $this->security->xss_clean(strip_tags(trim($this->input->post('password'))));

				// Get User details bu Username & Password
				$getUserDetails = $this->user->loginCheck($userData); 

				if($getUserDetails) {
					$this->session->set_userdata('userID', $getUserDetails['id']);
					$this->session->set_userdata('userType', $getUserDetails['user_type']);
					redirect('dashboard', 'refresh'); 
				} else {
					$errMsg = "Invalid login details provided.";
					$this->session->set_flashdata('error_msg', $errMsg);
					redirect('login'); 
				}
				
			} else {
				$errMsg = "There is an error on form submission. Please correct and submit again!";
				$this->session->set_flashdata('error_msg', $errMsg);
				redirect('login'); 
			}
			
		} else {
			// Load the Login Screen view
			$this->load->view('layout/header');
			$this->load->view('layout/nav');
			$this->load->view('general/login', $data);
			$this->load->view('layout/footer');
		}	
	}

	/*
		Description: User Sign Out
		Purpose: Logout from application & destroy the session
		Created By: Manish Bera
		Date of Creation: 22-May-2024
	*/
	public function signout()
	{
		if($this->userid){
			$this->session->sess_destroy(); // Destroying the Active Session(s) 
			redirect('login', 'refresh'); // Redirect to the Login Page
		} 
	}
	
	
	

}
