<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class User extends CI_Model{ 

    function __construct() { 
        parent::__construct();
        // Set table name 
        $this->table = 'user'; 
    } 

    /* 
    * Check already exists the same user data into the database 
    * @param $data data to be inserted 
    */ 
    public function getRows($str){
        if(!empty($str)){ 
            $chkUserData = $this->db->like('email', $str, 'both')->get($this->table);
            return $chkUserData->num_rows();
        } else {

        }
    }
    
    /* 
    * Insert user data into the database 
    * @param $data data to be inserted 
    */ 
    public function insert($data = array()) { 
        if(!empty($data)){ 
            // Add created and modified date if not included 
            if(!array_key_exists("created_at", $data)){ 
                $data['created_at'] = date("Y-m-d H:i:s"); 
            } 
            if(!array_key_exists("modified_at", $data)){ 
                $data['modified_at'] = date("Y-m-d H:i:s"); 
            } 
             
            // Insert user data to databse table
            $insert = $this->db->insert($this->table, $data); 
             
            // Return the status 
            return $insert?$this->db->insert_id():false; 
        } 
        return false; 
    } 

    /* 
    * Login details check with Username & Password
    * @param $data to be checked 
    */ 
    public function loginCheck($data = array()) { 
        if(!empty($data)){ 
            $this->db->select('id,name,username,email,user_type,status') 
                    ->where('email =', $data['username'])
                    ->or_where('username =', $data['username'])
                    ->where('password =', md5($data['password']));

            $chkUserData = $this->db->get($this->table);

            if($chkUserData->num_rows()==1){
                return $chkUserData->row_array();
            } else {
                return false;
            }
        }
    }

    /* 
    * Get User-Details by userid
    */ 
    public function getUserDetails($userID) { 
        if(!empty($userID)){ 
            $this->db->select('*')->where('id =', $userID);
            $getUserDetails = $this->db->get($this->table);

            if($getUserDetails->num_rows()==1){
                return $getUserDetails->row_array();
            } else {
                return false;
            }
        }
    }

    /* 
    * Check User-Password by userid
    */ 
    public function chkUserPassword($oldPass) {
        $userID = $this->session->userdata('userID');
        if($userID){ 
            $this->db->where(array('id'=>$userID, 'password'=>md5($oldPass))); 
            $recordCount = $this->db->count_all_results($this->table);          

            //die($this->db->last_query());

            if($recordCount>0){
                return true;
            } else {
                return false;
            }
        } else { 
            return false;
        }
    }

    /* 
    * Update User-Password by userid
    * @param $data to be updated 
    */ 
    public function updateUserDetails($data = array()) {
        $userID = $this->session->userdata('userID');
        if($userID){ 
            $updatePass = $this->db->where('id', $userID)->update($this->table, $data); 
            if($updatePass){
                return true;
            } else {
                return false;
            }
        } else { 
            return false;
        }
    }

    /* 
    * Total number of Users by UserType
    */ 
    public function getTotalUserCount($userType) {
        return $this->db->where('user_type', $userType)->count_all_results($this->table);             
    }   


}    