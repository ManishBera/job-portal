<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Job extends CI_Model{ 

    function __construct() { 
        parent::__construct();
        
        // Set table name 
        $this->table = 'jobs_list'; 

        // Retrive the UserId from session & store into a variable
		$this->userid = $this->session->userdata('userID');
    }   

    /* 
    * Insert new job details to database
    * @param $data to be checked 
    */ 
    public function addNewJobDetails($data = array()) { 
        if(!empty($data)){ 

            // Insert the Job Details to Database
            $insert = $this->db->insert($this->table, $data);
            
            // Return the status 
            return $insert?$this->db->insert_id():false;

        } else {
            return false;
        }
    } 
    
    /* 
    * Retrive Individual Employer job list from database 
    */ 
    public function getEmployerJobList() { 

        // Retrive the Job List based on EmployerID 
        $jobList = $this->db->get_where($this->table, array('employer_id'=>$this->userid));

        // Return the Job List Array
        return $jobList->result_array();
    }

    /* 
    * Retrive Job Details by JobID from database    
    */ 
    public function getJobDetails($jobID) { 

        // Retrive the Job Details based on JobID
        $jobDetails = $this->db->get_where($this->table, array('id'=>$jobID));

        // Return the Job Details Data 
        return $jobDetails->row_array();
    }

    /* 
    * Update Job Details by JobID to database    
    * @param $data to be checked 
    */ 
    public function updateJobDetails($data = array(), $jobID) { 

        if(!empty($data)){
            $update = $this->db->where('id', $jobID)->update($this->table, $data);         
            return  ($this->db->affected_rows()>=0) ? true : false;
        } else { 
            return false;
        }      
    }

    /* 
    * Delete Job Details by JobID to database    
    * @param $jobID to be checked 
    */ 
    public function deleteJobDetails($jobID) { 

        if(!empty($jobID)){
            $delete = $this->db->where('id', $jobID)->delete($this->table);         
            return  ($this->db->affected_rows()>=0) ? true : false;
        } else { 
            return false;
        }      
    }

    /* 
    * Retrive Job List from database 
    */ 
    public function getJobList($limit=null,$orderBy) { 

        // Retrive the Active Status Job List
        $this->db->where('status',1);
        if($limit){
            $this->db->limit($limit,0);
        }
        if($orderBy){
            $this->db->order_by('id', $orderBy);        
        }

        $jobList = $this->db->get($this->table);

        // Return the Job List Array
        return $jobList->result_array();
    }
    
    /* 
    * Total number of Company
    */ 
    public function getTotalCompanyCount() {
        return $this->db->select('company_name')->count_all_results($this->table);             
    }

    
}   