<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class User extends CI_Model{ 

    function __construct() { 
        parent::__construct();
        
        // Set table name 
        $this->table = 'resume_details'; 
    } 

    /* 
    * Get Loggedin User Resume Details by UserID
    * @param $data data to be inserted 
    */ 
    public function getRows(){
        $userID = $this->session->userdata('userID');
        if($userID){             
            $this->db->select('*')->where('id =', $userID);
            $getUserResumeDetails = $this->db->get($this->table);
            
            if($getUserResumeDetails->num_rows()>0){
                return $getUserResumeDetails->row_array();
            } else {
                return false;
            }
        } else {

        }
    }
}